# Scrisoare deschisă despre GovITHub

Stimate domnule prim-ministru Daniel Cioloș, stimați membrii a consiliui GovITHub,

Ne bucurăm de inițiativa de care a făcut dovada Guvernul prin lansarea GovITHub și considerăm că este un mod excelent pentru noi, cei din sfera IT să contribuim la bunul public din țară.

Pentru o colaborare eficientă vă rugăm să clarificați punctul Dumneavoastră de vedere în legătură cu două întrebări:

## Pozițiile de fellowship sunt limitate la București?

Pe pagina ”Despre Program” [^1] scrie ”fellowship (poziție remunerată competitiv, full-time, la București)”.

Considerăm că expertiza IT românească este distribuită în toată țara, și chiar în toată lumea. În aceste condiții ar fi păcat limitarea pozițiilor la București. Majoritatea lucrăm cu clienți din toată lumea (din SUA până în Tokyo) și avem o colaborare eficientă cu ei. Nu ar fi nici o dificultate să colaborăm cu colegi din țară de care ne separ doar câteva sute de kilometrii nu zeci de mii.

## Care este punctul de vedere a Guvernul despre dezvoltarea soluțiilor în mod transparent și publicarea acestora sub licențe de software libere [^2]?

Considerăm că dezvoltarea în mod transparent (publicarea planului în avans și publicarea rezultatelor intermediare în momentul creerii lor) rezultă într-un produs final de o calitate mai înaltă decât alternativele pentru că oricine poate să ofere sugestii în timp ce se decurge proiectul, nu doar la final când schimbările sunt mai grele.

Totodată considerăm ca soluțiile trebuie dezvoltate în spiritul sofware-ului liber și publicate sub astfel de licențe din mai multe motive:

* Banii investiți în inițiativă provin din taxe, deci este de preferat ca toți cetățenii să beneficieze de rezultate
* Publicarea soluțiilor sub o licență liberă permite persoanelor dinafara GovITHub să propună și să aducă îmbunătățiri
* Disponibilitatea soluțiilor sub o licență liberă oferă o flexibilitate Guvernului: la orice moment poate să preia o altă echipă / companie / organizație menținerea și îmbunătățirea proiectului
* Soluțiile publicate sub o licență liberă pot fi refolosite și adaptate ușor de alte departamente
* Soluțiile publicate sub o licență liberă pot fi folosite de studenți să înțeleagă mai bine cerințele unui proiect real de IT și să capete experiență
* Există o multitudine de organizații la nivel național și internațional care ajută proiectele publicate sub licențe libere cu care am putea colabora

## În concluzie propunem ca:

* Toate pozițiile din cadrul GovITHub să fie disponibile tuturor cetățenilor
* Toate informațiile legate de GovITHub și proiectele propuse și acceptate să fie disponibile în mod public (în timp real)
* Proiectele să fie dezvoltate în mod transparent, cu toate rezultatele intermediare disponibile publicului
* Proiectele să fie disponibile sub o licență liberă pentru a maximiza impactul pozitiv

[^1]: [http://ithub.gov.ro/despre-noi/](http://ithub.gov.ro/despre-noi/)
[^2]: [https://www.gnu.org/philosophy/free-sw.ro.html](https://www.gnu.org/philosophy/free-sw.ro.html)

Semnatarii,  
(accesați [SEMNATARII](SEMNATARII.md) pentru o listă completă)

