Scrisoare deschisă către domnul prim-ministru Dacian Cioloș despre inițiativa GovITHub
===

Citiți scrisoarea: [SCRISOARE](SCRISOARE.md)

Vedeți lista semnatarilor: [SEMNATARII](SEMNATARII.md)

Pentru a semna scrisoarea, trimiteți o cerere de schimbare ("merge request" [^1]) pentru fișierul SEMNATARII.md.

Pentru a propune o modificare la conținutul scrisorii, trimiteți o cere de schimbare ("merge request") pentru fișierul SCRISOARE.md.

[^1]: Echivalentul "pull request"-ului de pe GitHub. Cel mai simplu este să navigați la fișier și (după ce vă creați / vă logați în contul de GitLab) apăsați tasta "Edit" dreapta-sus
